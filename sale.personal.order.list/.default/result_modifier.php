<?
use Bitrix\Sale;

foreach ($arResult['ORDERS'] as $key => &$order)
{
	// получение списка свойств для заказа
	$dbRes = \Bitrix\Sale\PropertyValueCollection::getList([
	    'select' => ['*'],
	    'filter' => [
	        '=ORDER_ID' => $order['ORDER']['ID'], 
	    ]
	]);
	$order['PROPS'] = [];
	while ($item = $dbRes->fetch())
	{
	    $order['PROPS'][$item['CODE']] = $item;
	}
	/// items
	foreach ($order['BASKET_ITEMS'] as $key => &$arItem) {
		$res = CIBlockElement::GetByID($arItem['PRODUCT_ID']);
		if($arItem['ELEMENT'] = $res->GetNext())
			$arItem["PREVIEW_PICTURE_RESIZE"] = CFile::ResizeImageGet($arItem['ELEMENT']["PREVIEW_PICTURE"], array('width'=>50, 'height'=>50), BX_RESIZE_IMAGE_EXACT, false, false, false, 70);
	}
}