// Акордион заказов

$(function() {
    var order = $('.orders-history').find('.order');
    var link = order.find('.order__header');

    link.on('click', function() {
        order.not($(this).parent()).removeClass('order_opened');
        $(this).parent().toggleClass('order_opened').find('.order__body').slideToggle();
    });

});
