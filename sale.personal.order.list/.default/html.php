<?

use Bitrix\Main,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset;
//print_r($arResult['INFO']['STATUS']);

$arResult['INFO']['STATUS']['N']['NAME'] = 'Ждет оплаты';
$arResult['INFO']['STATUS']['P']['NAME'] = 'Оплачен';

?>
					<div class="orders-history">
				<?
				if ($_REQUEST["filter_history"] !== 'Y')
				{
					$paymentChangeData = array();

					foreach ($arResult['ORDERS'] as $key => $order)
					{
						//print_r($order);
						$orderHeaderStatus = $order['ORDER']['STATUS_ID'];
						?>
						<div class="order">
							<div class="order__header">
								<div class="row row_order">
									<div class="col-lg-2">
										<div class="order__id-block">
											<div class="order__arrow"></div>
											<div class="order__id">№ <?=$order['ORDER']['ACCOUNT_NUMBER']?></div>
										</div>
									</div>
									<div class="col-lg-2">
										<div class="order__date"><?=$order['ORDER']['DATE_INSERT']->format($arParams['ACTIVE_DATE_FORMAT'])?></div>
									</div>
									<div class="col-lg-3">
										<div class="order__items">
											<?foreach ($order['BASKET_ITEMS'] as $key => $arItem) {?>
											<div class="order__item">
												<img loading="lazy" class="order__item-img" src="<?=$arItem["PREVIEW_PICTURE_RESIZE"]['src']?>" alt="<?=$arItem['NAME']?>">
											</div>
											<?}?>
										</div>
									</div>
									<div class="col-lg-3">
										<div class="order__description">
											<span class="order__count-item">
												<?=count($order['BASKET_ITEMS']);?> 
												<?
												$count = count($order['BASKET_ITEMS']) % 10;
												if ($count == '1')
												{
													echo Loc::getMessage('SPOL_TPL_GOOD');
												}
												elseif ($count >= '2' && $count <= '4')
												{
													echo Loc::getMessage('SPOL_TPL_TWO_GOODS');
												}
												else
												{
													echo Loc::getMessage('SPOL_TPL_GOODS');
												}
												?>
											</span>
											<span> на сумму</span>
											<span class="order__sum"><?=$order['ORDER']['FORMATED_PRICE']?></span>
										</div>
									</div>
									<div class="col-lg-2">
										<div class="order__status order__status_complete"><?=htmlspecialcharsbx($arResult['INFO']['STATUS'][$orderHeaderStatus]['NAME'])?></div>
									</div>
								</div>
							</div>
							<div class="order__body">
								<div class="row">
									<div class="col-lg-4">
										<div class="order__info">
											<div class="order__info-field">
												<div class="order__info-field-title">Получатель</div>
												<div class="order__info-field-content"><?=$order['PROPS']['FIO']['VALUE']?></div>
											</div>
											<div class="order__info-field">
												<div class="order__info-field-title"><?=$order['PROPS']['PHONE']['NAME']?></div>
												<div class="order__info-field-content"><a href="tel:<?=$order['PROPS']['PHONE']['VALUE']?>"><?=$order['PROPS']['PHONE']['VALUE']?></a></div>
											</div>
											<div class="order__info-field">
												<div class="order__info-field-title"><?=$order['PROPS']['EMAIL']['NAME']?></div>
												<div class="order__info-field-content"><a href="mailto:<?=$order['PROPS']['EMAIL']['VALUE']?>"><?=$order['PROPS']['EMAIL']['VALUE']?></a></div>
											</div>
											<div class="order__info-field">
												<div class="order__info-field-title"><?=$order['PROPS']['ADDRESS']['NAME']?></div>
												<div class="order__info-field-content"><?=$order['PROPS']['ADDRESS']['VALUE']?></div>
											</div>
										</div>
									</div>
									<div class="col-lg-8">
										<div class="ordered-items">
											<div class="ordered-items__list">
												<?foreach ($order['BASKET_ITEMS'] as $key => $arItem) {?>
												<div class="ordered-item">
													<div class="row ordered-item__row">
														<div class="col-lg-5 col-xs-12">
															<div class="ordered-item__info">
																<div class="ordered-item__img-block">
																	<img loading="lazy" src="<?=$arItem["PREVIEW_PICTURE_RESIZE"]['src']?>" alt="<?=$arItem['NAME']?>" class="ordered-item__img">
																</div>
																<a href="<?=$arItem['ELEMENT']['DETAIL_PAGE_URL']?>" class="ordered-item__title"><?=$arItem['NAME']?></a>
															</div>
														</div>
														<div class="col-lg-3 col-xs-4">
															<div class="ordered-item__cost"><?=$arItem['NAME']?></div>
														</div>
														<div class="col-lg-1 col-xs-4">
															<div class="ordered-item__count"><?=$arItem['QUANTITY']?> шт.</div>
														</div>
														<div class="col-lg-3 col-xs-4">
															<div class="ordered-item__cost_total"><?=CurrencyFormat($arItem['PRICE'], $arItem['CURRENCY']);?> </div>
														</div>
													</div>
												</div>
												<?}?>
											</div>
											<div class="ordered-items__total">
												<div class="row">
													<div class="col-sm-6">
														<div class="ordered-items__total-count">
															В заказе <span><?=count($order['BASKET_ITEMS']);?> </span>
															<?
															$count = count($order['BASKET_ITEMS']) % 10;
															if ($count == '1')
															{
																echo Loc::getMessage('SPOL_TPL_GOOD');
															}
															elseif ($count >= '2' && $count <= '4')
															{
																echo Loc::getMessage('SPOL_TPL_TWO_GOODS');
															}
															else
															{
																echo Loc::getMessage('SPOL_TPL_GOODS');
															}
															?>
														</div>		
													</div>
													<div class="col-sm-6">
														<div class="ordered-items__total-cost">
															Итого к оплате: <span class="bold"><?= $order['ORDER']['FORMATED_PRICE'] ?></span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?
					}
				}?>
					</div>
